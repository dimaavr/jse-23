package ru.tsc.avramenko.tm.component;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.repository.*;
import ru.tsc.avramenko.tm.api.service.*;
import ru.tsc.avramenko.tm.command.AbstractCommand;
import ru.tsc.avramenko.tm.command.auth.LoginCommand;
import ru.tsc.avramenko.tm.command.auth.LogoutCommand;
import ru.tsc.avramenko.tm.command.project.*;
import ru.tsc.avramenko.tm.command.system.*;
import ru.tsc.avramenko.tm.command.task.*;
import ru.tsc.avramenko.tm.command.user.*;
import ru.tsc.avramenko.tm.enumerated.Role;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import ru.tsc.avramenko.tm.exception.system.UnknownCommandException;
import ru.tsc.avramenko.tm.model.Project;
import ru.tsc.avramenko.tm.model.Task;
import ru.tsc.avramenko.tm.model.User;
import ru.tsc.avramenko.tm.repository.*;
import ru.tsc.avramenko.tm.service.*;
import ru.tsc.avramenko.tm.util.TerminalUtil;

import java.util.Objects;
import java.util.Optional;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final IAuthRepository authRepository = new AuthRepository();

    @NotNull
    private final IAuthService authService = new AuthService(authRepository, userService);


    public void start(String[] args) {
        displayWelcome();
        initUsers();
        initData();
        parseArgs(args);
        process();
    }

    {
        registry(new DisplayCommand());
        registry(new AboutDisplayCommand());
        registry(new ExitCommand());
        registry(new HelpCommand());
        registry(new InfoDisplayCommand());
        registry(new VersionDisplayCommand());
        registry(new ArgumentsDisplayCommand());

        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectChangeStatusByNameCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectFinishByIdCommand());
        registry(new ProjectFinishByIndexCommand());
        registry(new ProjectFinishByNameCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectShowByNameCommand());
        registry(new ProjectShowListCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByNameCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskBindToProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskChangeStatusByNameCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskFinishByIdCommand());
        registry(new TaskFinishByIndexCommand());
        registry(new TaskFinishByNameCommand());
        registry(new TaskListByProjectIdCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByNameCommand());
        registry(new TaskShowListCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByNameCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());

        registry(new LoginCommand());
        registry(new LogoutCommand());

        registry(new UserShowListCommand());
        registry(new UserCreateCommand());
        registry(new UserClearCommand());
        registry(new UserChangeRoleCommand());
        registry(new UserShowByIdCommand());
        registry(new UserShowByLoginCommand());
        registry(new UserPasswordChangeCommand());
        registry(new UserRemoveByIdCommand());
        registry(new UserRemoveByLoginCommand());
        registry(new UserUpdateByIdCommand());
        registry(new UserUpdateByLoginCommand());
        registry(new UserLockByLoginCommand());
        registry(new UserUnlockByLoginCommand());
    }

    public void initUsers() {
        userService.create("Test", "Test", Role.USER);
        userService.updateUserByLogin("Test", "Testoviy", "Test", "Testovich", "test@gmail.com");
        userService.create("Admin", "Admin", Role.ADMIN);
        userService.updateUserByLogin("Admin", "Adminov", "Admin", "Adminovich", "admin@gmail.com");
    }

    private void initData() {
        projectService.create(Objects.requireNonNull(userRepository.findByLogin("Test")).getId(), "Project C", "1");
        projectService.create(Objects.requireNonNull(userRepository.findByLogin("Test")).getId(), "Project A", "2");
        projectService.create(Objects.requireNonNull(userRepository.findByLogin("Admin")).getId(), "Project B", "3");
        projectService.create(Objects.requireNonNull(userRepository.findByLogin("Admin")).getId(), "Project D", "4");
        taskService.create(Objects.requireNonNull(userRepository.findByLogin("Test")).getId(), "Task C", "1");
        taskService.create(Objects.requireNonNull(userRepository.findByLogin("Test")).getId(), "Task A", "2");
        taskService.create(Objects.requireNonNull(userRepository.findByLogin("Admin")).getId(), "Task B", "3");
        taskService.create(Objects.requireNonNull(userRepository.findByLogin("Admin")).getId(), "Task D", "4");
        projectService.finishByName(Objects.requireNonNull(userRepository.findByLogin("Test")).getId(), "Project C");
        projectService.startByName(Objects.requireNonNull(userRepository.findByLogin("Admin")).getId(), "Project B");
        taskService.finishByName(Objects.requireNonNull(userRepository.findByLogin("Test")).getId(), "Task C");
        taskService.startByName(Objects.requireNonNull(userRepository.findByLogin("Admin")).getId(), "Task B");
    }

    private void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private void displayAuth() {
        System.out.println("\n" + TerminalUtil.ANSI_RED + "You are not authorized. " + "Enter one of the following commands: " + TerminalUtil.ANSI_RESET + "\n" + "\n"
                + commandService.getCommandByName("login") + "\n"
                + commandService.getCommandByName("exit") + "\n"
                + commandService.getCommandByName("user-create") + "\n"
                + commandService.getCommandByName("about") + "\n"
                + commandService.getCommandByName("version") + "\n");
    }

    private void process() {
        logService.debug("Logging started.");
        String exit = new ExitCommand().name();
        @NotNull
        String command = "";
        while (!exit.equals(command)) {
            try {
                while (!getAuthService().isAuth()) { //доступные команды для неавторизированных пользователей
                    try {
                        displayAuth();
                        command = TerminalUtil.nextLine();
                        switch (command) {
                            case "exit": parseCommand("exit"); break;
                            case "login": parseCommand("login"); break;
                            case "user-create": parseCommand("user-create"); break;
                            case "about": parseCommand("about"); break;
                            case "version": parseCommand("version"); break;
                            default: throw new AccessDeniedException();
                        }
                    } catch (@NotNull final Exception e) {
                        logService.error(e);
                    }
                }
                System.out.println("ENTER COMMAND:");
                command = TerminalUtil.nextLine();
                logService.debug(command);
                parseCommand(command);
                logService.debug("Completed.");
            } catch (@NotNull final Exception e) {
                logService.error(e);
            }
        }
    }

    public void parseArg(@Nullable final String arg) {
        @Nullable
        AbstractCommand arguments = commandService.getCommandByArg(arg);
        if (!Optional.ofNullable(arguments).isPresent()) throw new UnknownCommandException();
        arguments.execute();
    }

    public void parseCommand(@Nullable final String command) {
        if (!Optional.ofNullable(command).isPresent() || command.isEmpty()) return;
        @Nullable
        AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (!Optional.ofNullable(abstractCommand).isPresent()) throw new UnknownCommandException();
        final Role[] roles = abstractCommand.roles();
        authService.checkRoles(roles);
        abstractCommand.execute();
    }

    private void registry(@Nullable AbstractCommand command) {
        if (!Optional.ofNullable(command).isPresent()) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void parseArgs(@Nullable String[] args) {
        if (!Optional.ofNullable(args).isPresent() || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
        System.exit(0);
    }

    @NotNull
    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public IAuthService getAuthService() {
        return authService;
    }
}