package ru.tsc.avramenko.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.command.AbstractTaskCommand;
import ru.tsc.avramenko.tm.enumerated.Role;
import ru.tsc.avramenko.tm.exception.entity.TaskNotFoundException;
import ru.tsc.avramenko.tm.model.Task;
import ru.tsc.avramenko.tm.util.TerminalUtil;

public class TaskFinishByNameCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-finish-by-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Finish task by name.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        @NotNull final String userId = serviceLocator.getAuthService().getCurrentUserId();
        @Nullable final Task task = serviceLocator.getTaskService().finishByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        serviceLocator.getTaskService().finishByName(userId, name);
        System.out.println("[OK]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}