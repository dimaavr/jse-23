package ru.tsc.avramenko.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.command.AbstractCommand;

public class AboutDisplayCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "about";
    }

    @Nullable
    @Override
    public String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public String description() {
        return "Display developer information.";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("DEVELOPER: Avramenko Dmitry");
        System.out.println("E-MAIL: DimaAvramenko@yandex.ru");
    }

}