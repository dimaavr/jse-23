package ru.tsc.avramenko.tm.api;

import ru.tsc.avramenko.tm.model.AbstractEntity;

import java.util.List;

public interface IService <E extends AbstractEntity> extends IRepository<E> {
}